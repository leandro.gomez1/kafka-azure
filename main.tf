provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "kafka_rg" {
  name     = "kafka-rg"
  location = "East US"
}

resource "azurerm_kubernetes_cluster" "kafka_cluster" {
  name                = "kafka-cluster"
  location            = azurerm_resource_group.kafka_rg.location
  resource_group_name = azurerm_resource_group.kafka_rg.name
  dns_prefix          = "kafka-cluster"
  agent_pool_profile {
    name            = "agentpool"
    count           = 1
    vm_size         = "Standard_D2_v2"
    os_type         = "Linux"
  }
  service_principal {
    client_id     = var.client_id
    client_secret = var.client_secret
  }
  tags = {
    Environment = "POC"
  }
}

resource "kubernetes_namespace" "kafka_namespace" {
  metadata {
    name = "kafka"
  }
}

resource "kubernetes_secret" "kafka_secret" {
  metadata {
    name      = "kafka-secret"
    namespace = kubernetes_namespace.kafka_namespace.metadata[0].name
  }
  data = {
    "tls.crt" = filebase64("./certs/kafka.crt")
    "tls.key" = filebase64("./certs/kafka.key")
  }
  type = "Opaque"
}

resource "kubernetes_service" "kafka_service" {
  metadata {
    name      = "kafka"
    namespace = kubernetes_namespace.kafka_namespace.metadata[0].name
  }
  spec {
    selector = {
      app = "kafka"
    }
    type = "LoadBalancer"
    load_balancer_ip = azurerm_public_ip.kafka_public_ip.ip_address
    ports {
      name       = "tcp"
      port       = 9092
      target_port = 9092
    }
  }
}

resource "azurerm_public_ip" "kafka_public_ip" {
  name                = "kafka-public-ip"
  location            = azurerm_resource_group.kafka_rg.location
  resource_group_name = azurerm_resource_group.kafka_rg.name

  allocation_method = "Static"

  tags = {
    environment = "POC"
  }
}

resource "kubernetes_deployment" "kafka_deployment" {
  metadata {
    name      = "kafka"
    namespace = kubernetes_namespace.kafka_namespace.metadata[0].name
    labels = {
      app = "kafka"
    }
  }
  spec {
    selector {
      match_labels = {
        app = "kafka"
      }
    }
    replicas = 1
    template {
      metadata {
        labels = {
          app = "kafka"
        }
      }
      spec {
        container {
          name            = "kafka"
          image           = "confluentinc/cp-kafka:5.5.1"
          image_pull_policy = "Always"
          ports {
            container_port = 9092
          }
        }
      }
        env {
            name  = "KAFKA_ADVERTISED_LISTENERS"
            value = "PLAINTEXT"      
        env {
            name  = "KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR"
            value = "1"
        }
        env {
            name  = "KAFKA_SSL_KEYSTORE_FILENAME"
            value = "/etc/kafka/secrets/kafka.key"
        }
        env {
            name  = "KAFKA_SSL_KEYSTORE_PASSWORD"
            value = "${var.keystore_password}"
        }
        env {
            name  = "KAFKA_SSL_TRUSTSTORE_FILENAME"
            value = "/etc/kafka/secrets/kafka.truststore.jks"
        }
        env {
            name  = "KAFKA_SSL_TRUSTSTORE_PASSWORD"
            value = "${var.truststore_password}"
        }
        volume_mount {
            name       = "kafka-secrets"
            mount_path = "/etc/kafka/secrets"
        }
        }
        image_pull_secrets {
        name = "confluent-registry"
        }
        volume {
        name = "kafka-secrets"
        secret {
            secret_name = kubernetes_secret.kafka_secret.metadata[0].name
        }
        }
    }
    }
}